#!/bin/bash

[[ ! ${tools} ]] && export tools=tools

. ${tools}/common

# test -d ${cache}/git-pkgs && exit 0

if [[ "${repo_mode}" == "cdrom" ]] ; then
    touch ${cache}/git-pkgs
    exit $?
fi

UNREAL_PKGS=";fdi;fdi-x86;fdinst;fdisrc;"

test -d ${cache}/packages || mkdir -p ${packages}
test -d ${cache}/packages || die

function search_lfn () {
    local s
    local f
    local o="${1}"

    local n
    local e
    [[ "${o}" != "" ]] && o="${o}/"
    # echo $o
    for s in "${o}"* ; do
        f="${s##*/}"
        n="${f%%.*}"
        # $f & $e equal then No Extension - OK
        [[ "${n}" == "${f}" ]] && e="" || e="${f#*.}"

        # Only Extension, Name > 8 or Ext > 3 then is LFN, or a dot in the extenstion
        if [[ ${#n} -eq 0 ]] || [[ ${#n} -gt 8 ]] || [[ ${#e} -gt 3 ]] || [[ "${e//.}" != "${e}" ]]; then
            echo "${s}"
        elif [[ -d "${s}" ]] ; then
            search_lfn "${s}"
        fi
    done
}

function lfn_dir () {
    pushd "${1}" >/dev/null || return 1
    local line flag
    search_lfn "" | while IFS=""; read line ; do
        [[ "$flag" == "" ]] && {
            flag=yes
            isVerbose "$(echo ${PWD##*/} | tr [:lower:] [:upper:]) contain LFNs... Compressing"
        }
        # echo ${1}/${line}
        zip -q -r -9 -m -o LFNFILES.ZIP "${line}" || return 1
    done
    popd >/dev/null
}

function lfn_subdirs () {
    local d
    for d in "${1}"/* ; do
        [[ ! -d "${d}" ]] && continue
        lfn_dir "${d}" || return 1
    done
    return 0
}

function make_lfnfiles () {
    local d dw
    for d in * ; do
        [[ ! -e "${d}" ]] && break
        dw=$(lowerCase "${d##*/}")
        if [[ "${dw}" == "source" ]] ; then
            continue
        elif [[ "${dw}" == "bin" ]] || [[ "${dw}" == "links" ]] ; then
            lfn_dir "${d}" || return 1
        else
            lfn_subdirs "${d}" || return 1
        fi
    done
    return 0
}

function exclude_compress () {
    local OK_EXT=";ZIP;TXT;ME;MD;DOC;7Z;"
    local OK_NAME=";README;READ;SOURCE;SOURCES;"
    local f n e t
    for f in * ; do
        # Test for empty directory
        [[ ! -e "${f}" ]] && return 0
        # any subdirectories, then compress
        [[ -d "${f}" ]] && return 1
        
        # adjust case and split filename into name & extesion
        f=$(upperCase "${f}")
        n="${f%%.*}"
        # $f & $e equal then No Extension - OK
        [[ "${n}" == "${f}" ]] && e="" || e="${f#*.}"
  
        # if any LFN's then must compress sources
        # Only Extension, Name > 8 or Ext > 3 then is LFN, or a dot in the extenstion
        if [[ ${#n} -eq 0 ]] || [[ ${#n} -gt 8 ]] || [[ ${#e} -gt 3 ]] || [[ "${e//.}" != "${e}" ]]; then
            return 1
        fi
        
        # if extension is in list, then OK so far
        t="${OK_EXT}"
        [[ "${t//;${e};}" != "${t}" ]] && continue

        # if name is in list, then OK so far
        t="${OK_NAME}"
        [[ "${t//;${n};}" != "${t}" ]] && continue
        
        # otherwise, we need to compress sources
        return 1
    done
    return 0    
    # returns 0  do not compress sources
    # returns !0 to compress sources
}

function git_package_task () {
    local line="${1}"

    # Some path and file remapping for gitlab is necessary
    local prjp="${line%/*}"
    case "${prjp}" in
        'edit') prjp='editor';;
    esac
    local prjn="${line##*/}"
    case "${prjn}" in
        'edit') prjn='edit-freedos';;
        'tree') prjn='tree-freedos';;
    esac

    echo "clone project ${prjp}/${prjn}.git as package ${line##*/}"
    echo "clone package ${line##*/} process" >&2
    GIT_TERMINAL_PROMPT=0 git clone "${git_pkg}/${prjp}/${prjn}.git" "${line##*/}" >/dev/null 2>&1 || return 1
    echo "  cloned" >&2
    cd "${line##*/}" || return 1
    if [[ ${rbe_devel} ]] ; then
        git checkout ${rbe_devel} >/dev/null 2>&1
        if [[ $? -eq 0 ]] ; then
            echo "Warning: ${line##*/} switched to ${rbe_devel} branch" | errorlog
            git pull >/dev/null 2>&1
        fi
    fi
    isVerbose "adjust time stamps for ${line##*/}"
    fdvcs.sh -jat -q -s || return 1
    echo "  timestamps adjusted" >&2

    isVerbose "log changes to ${line##*/}"
    log_changes "${line##*/}" || return 1

    local sdir=$(fileCase "source/${line##*/}")
    if [[ -d "${sdir}" ]] ; then
        cd "${sdir}" || return 1
        exclude_compress
        if [[ $? -eq 0 ]] ; then 
            isVerbose "exclude compressing package source files"
            cd ../.. || return 1
        else        
            isVerbose "compress package source files"
            zip -q -r -9 -o ../../SOURCES.ZIP * || return 1
            cd ../.. || return 1
            rm -rf $(fileCase "source") || return 1
            mkdir -p $(upperCase "source/${line##*/}") || return 1
            mv SOURCES.ZIP $(upperCase "source/${line##*/}/") || return 1
            echo "  compressed sources" >&2
        fi
    else
        isVerbose echo "package ${line##*/} has no source files"
    fi

    for i in * ; do
        if [[ -f "${i}" ]] ; then
            rm -f "${i}" || return 1
        fi
    done
    make_lfnfiles || return 1
    echo "  any/all LFN files compressed" >&2

    isVerbose "compress ${line##*/} package"
    zip -q -r -9 -o "../${line##*/}.zip" * || return 1
    echo "  compressed" >&2

    return 0
}

function git_package_job () {
    local tout="${1}"
    shift
    git_package_task "${1}"
    ret=$?
    rm -rf "${swd}/${packages}/${1}" || ret=$?
    if [[ ${ret} -ne 0 ]] ; then
        [[ -e "${swd}/${packages}/${1}.zip" ]] && rm "${swd}/${packages}/${1}.zip"
        rm "${swd}/${cache}/git-pkgs" >/dev/null 2>&1
        echo "error creating package ${1}">>"${tout}"
        echo "error creating package ${1}"
        mv "${tout}" "${swd}/${temp}/threads/FAILED.${tout##*.}"
        exit 1
    else
        rm -rf "${tout}"
    fi
    return 0
}

function fetch_all_git_pkgs () {

    SHOW_ONCE=yes

    local i line t
    for i in ${cache}/fdi/SETTINGS/*.LST ${cache}/fdi-x86/SETTINGS/*.LST ; do
        [[ "${i//X86_KILL.LST}" != "${i}" ]] && continue
        [[ "${i//CLEANUP.LST}" != "${i}" ]] && continue
        grep -iv '^;\|^#\|^\$' "${i}" | grep -v '^[[:space:]]*$'
    done >${temp}/git-pkgs

    # these are the automatically added and the must have packages
    echo 'base\freecom' >> ${temp}/git-pkgs
    echo 'base\kernel' >> ${temp}/git-pkgs
    echo 'drivers\eltorito' >> ${temp}/git-pkgs

    cat ${temp}/git-pkgs | crlf | tr "[:upper:]" "[:lower:]" | sort -u >${cache}/git-pkgs
    # rm ${temp}/git-pkgs

    thread_clear || die

    # xx=0
    while read -r line ; do
        # (( xx++ ))
        # [[ ${xx} -gt 10 ]] && continue
        line="${line//\\/\/}"
        line="${line//[[:cntrl:]]}"
        [[ "${line##*/}" == "${line}" ]] && continue
        if [[ ! -d "${packages}/${line%/*}" ]] ; then
            mkdir -p ${packages}/${line%/*} || die
        fi
        if [[ -d "${line}" ]] ; then
            rm -rf "${line}"
            rm -rf "${packages}/${line}.zip"
        fi
        [[ -d "${line}" ]] && die
        [[ -f "${packages}/${line}.zip" ]] && continue

        [[ "${UNREAL_PKGS//${line##*/}}" != "${UNREAL_PKGS}" ]] && continue

        if [[ "${SHOW_ONCE}" != '' ]] ; then
            new_release_needed
            rm -f ${cache}/metaflag >/dev/null
            touch ${cache}/git-pkgs
            echo "Clone GitLab Repository source packages"
            unset SHOW_ONCE
        fi

        t=$(lowerCase "${line##*/}")
        # echo "${metadata}/${t}"
        rm -rf "${metadata}/${t}".* >/dev/null

        pushd "${packages}/${line%/*}" >/dev/null || die
        thread_spawn git_package_job "${line}"
        ret=$?
        popd >/dev/null || ret=$?
        [[ ${ret} -eq 0 ]] && thread_check
    done <${cache}/git-pkgs

    thread_wait "waiting for package retrieval and compression to complete"
}

function user_packages () {

    local i j

    lowerCaseFiles "${temp}/packages" || die
    SHOW_ONCE=yes
    for i in packages/* ; do
        if [[ -d "${i}" ]] ; then
            mkdir -p $(isVerbose) ${cache}/${i} || die
            for j in ${i}/* ; do
            	[[ ! -f "${j}" ]] && continue
                [[ -e "${cache}/${j}" ]] && continue
                if [[ "${SHOW_ONCE}" != '' ]] ; then
                    new_release_needed
                    rm -f ${cache}/metaflag >/dev/null
                    touch ${cache}/git-pkgs
                    echo "Copying custom user packages"
                    unset SHOW_ONCE
                fi

	            cp -fav "${j}" "${cache}/${j}" || die
            done
        fi
    done
    return 0
}

user_packages

fetch_all_git_pkgs

# rm ${cache}/git-pkgs
# touch ${cache}/git-pkgs &&

exit $?
